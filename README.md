# BattyBovine Custom Nodes
A small collection of custom Godot nodes.

Notes:
======
The SVG icon files at the root of the module directory need to be copied to `editor/icons` in the engine's source directory before recompiling the engine if you want the custom icons to appear. It may also be necessary to delete `editor/editor_icons.gen.h` so that `scons` can regenerate the file.
