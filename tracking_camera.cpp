#include "tracking_camera.h"

TrackingCamera::TrackingCamera() {
	this->nodeTarget = NULL;
	this->bIsEnabled = false;
	this->vVelocityMultiplier = this->vTimeMultiplier = Vector3(1.0, 1.0, 1.0);

	this->set_process_internal(false);
	this->set_physics_process_internal(true);
}

void TrackingCamera::_notification(int p_what) {
	switch (p_what) {
		case NOTIFICATION_ENTER_TREE: {
			this->find_node();
			this->vAnchor = this->get_translation();
		} break;
		case NOTIFICATION_INTERNAL_PROCESS: {			// Used when the target provides a velocity method we can check for
			if (this->nodeTarget && this->bIsEnabled && !Engine::get_singleton()->is_editor_hint()) {
				Vector3 node_translation = this->nodeTarget->get_translation();
				Vector3 target_velocity = Vector3(this->nodeTarget->call(this->sVelocityMethod)) / 100.0f;
				this->translate_camera(node_translation, target_velocity, this->get_process_delta_time());
				this->vPrevNodePos = node_translation;
			}
		} break;
		case NOTIFICATION_INTERNAL_PHYSICS_PROCESS: {	// Used when the target doesn't have its own velocity method
			if(this->nodeTarget && this->bIsEnabled && !Engine::get_singleton()->is_editor_hint()) {
				Vector3 node_translation = this->nodeTarget->get_translation();
				Vector3 target_velocity = node_translation - this->vPrevNodePos;
				this->translate_camera(node_translation, target_velocity, this->get_physics_process_delta_time());
				this->vPrevNodePos = node_translation;
			}
		} break;
	}
}

void TrackingCamera::translate_camera(Vector3 targettrans, Vector3 targetvel, real_t delta) {
	Vector3 self_translation = this->get_translation() - this->vPrevNodePos;
	self_translation.x = Math::lerp(float(self_translation.x), float(this->vAnchor.x + this->vAnchorOffset.x + (targetvel.x * this->vVelocityMultiplier.x)), float(delta * this->vTimeMultiplier.x));
	self_translation.y = Math::lerp(float(self_translation.y), float(this->vAnchor.y + this->vAnchorOffset.y + (-Math::abs(targetvel.y) * this->vVelocityMultiplier.y)), float(delta * this->vTimeMultiplier.y));
	self_translation.z = Math::lerp(float(self_translation.z), float(this->vAnchor.z + this->vAnchorOffset.z + Math::abs(targetvel.x * this->vVelocityMultiplier.z)), float(delta * this->vTimeMultiplier.z));
	this->set_translation(targettrans + self_translation);
}

void TrackingCamera::find_node() {
	if(!this->nodeTargetPath.is_empty() && this->has_node(this->nodeTargetPath))
		this->nodeTarget = Object::cast_to<Spatial>(this->get_node(this->nodeTargetPath));
	if(this->nodeTarget && this->nodeTarget->has_method(this->sVelocityMethod)) {
		this->set_process_internal(true);
		this->set_physics_process_internal(false);
	} else {
		this->set_process_internal(false);
		this->set_physics_process_internal(true);
	}
}

void TrackingCamera::reset_position() {
	if(this->nodeTarget) {
		Vector3 nodepos = this->nodeTarget->get_translation();
		this->set_translation(nodepos + this->vAnchor + this->vAnchorOffset);
		this->vPrevNodePos = nodepos;
	}
}

void TrackingCamera::_set_target(const Object *p_target) {
	ERR_FAIL_NULL(p_target);
	this->set_target(Object::cast_to<Spatial>(p_target));
}
void TrackingCamera::set_target(const Spatial *p_target) {
	ERR_FAIL_NULL(p_target);
	this->nodeTargetPath = get_path_to(p_target);
	this->set_target_node(this->nodeTargetPath);
}


void TrackingCamera::_bind_methods() {
	ClassDB::bind_method(D_METHOD("reset_position"), &TrackingCamera::reset_position);

	ClassDB::bind_method(D_METHOD("set_enabled", "target_path"), &TrackingCamera::set_enabled);
	ClassDB::bind_method(D_METHOD("is_enabled"), &TrackingCamera::is_enabled);
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "enabled"), "set_enabled", "is_enabled");

	ClassDB::bind_method(D_METHOD("set_target_node", "target_node"), &TrackingCamera::set_target_node);
	ClassDB::bind_method(D_METHOD("get_target_node"), &TrackingCamera::get_target_node);
	ClassDB::bind_method(D_METHOD("set_target", "target_node"), &TrackingCamera::_set_target);
	ADD_PROPERTY(PropertyInfo(Variant::NODE_PATH, "target_node"), "set_target_node", "get_target_node");

	ClassDB::bind_method(D_METHOD("set_velocity_multiplier"), &TrackingCamera::set_velocity_multiplier);
	ClassDB::bind_method(D_METHOD("get_velocity_multiplier"), &TrackingCamera::get_velocity_multiplier);
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR3, "velocity_multiplier"), "set_velocity_multiplier", "get_velocity_multiplier");

	ClassDB::bind_method(D_METHOD("set_time_multiplier"), &TrackingCamera::set_time_multiplier);
	ClassDB::bind_method(D_METHOD("get_time_multiplier"), &TrackingCamera::get_time_multiplier);
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR3, "time_multiplier"), "set_time_multiplier", "get_time_multiplier");

	ClassDB::bind_method(D_METHOD("set_velocity_method"), &TrackingCamera::set_velocity_method);
	ClassDB::bind_method(D_METHOD("get_velocity_method"), &TrackingCamera::get_velocity_method);
	ADD_PROPERTY(PropertyInfo(Variant::STRING, "velocity_method"), "set_velocity_method", "get_velocity_method");


	ClassDB::bind_method(D_METHOD("set_anchor_offset"), &TrackingCamera::set_anchor_offset);
	ClassDB::bind_method(D_METHOD("get_anchor_offset"), &TrackingCamera::get_anchor_offset);
}
