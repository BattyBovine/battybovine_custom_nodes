#ifndef WORLD_REFLECTION_PROBE_H_F2ECAA70E12F11E89F32F2801F1B9FD1
#define WORLD_REFLECTION_PROBE_H_F2ECAA70E12F11E89F32F2801F1B9FD1

#include <scene/3d/camera.h>
#include <scene/3d/reflection_probe.h>

class WorldReflectionProbe : public ReflectionProbe {
	GDCLASS(WorldReflectionProbe, ReflectionProbe)

public:
	WorldReflectionProbe();

	void set_energy_multiplier(real_t m) { this->rEnergyMultiplier=m; }
	_FORCE_INLINE_ real_t get_energy_multiplier() const { return this->rEnergyMultiplier; }
	void set_energy_exponent(real_t e) { this->rEnergyExponent=e; }
	_FORCE_INLINE_ real_t get_energy_exponent() const { return this->rEnergyExponent; }

protected:
	void _notification(int);
	static void _bind_methods();

private:
	Ref<Environment> envActive;
	real_t rEnergyMultiplier = 5.0;
	real_t rEnergyExponent = 5.0;
};

#endif	// WORLD_REFLECTION_PROBE_H_F2ECAA70E12F11E89F32F2801F1B9FD1
