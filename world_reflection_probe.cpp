#include "world_reflection_probe.h"

WorldReflectionProbe::WorldReflectionProbe() {
	this->set_process_internal(true);
}

void WorldReflectionProbe::_notification(int p_what) {
	switch (p_what) {
		case NOTIFICATION_INTERNAL_PROCESS: {
			if(!this->envActive.is_valid()) {
				Ref<World> world = this->get_viewport()->find_world();
				if(world.is_valid()) {
					this->envActive = world->get_environment();
					if(!this->envActive.is_valid()) {
						this->envActive = world->get_fallback_environment();
						if(!this->envActive.is_valid())
							return;
					}
				}
			}
			this->set_intensity(Math::pow(this->envActive->get_bg_energy()*this->rEnergyMultiplier, this->rEnergyExponent));
		} break;
	}
}


void WorldReflectionProbe::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_energy_multiplier"), &WorldReflectionProbe::set_energy_multiplier);
	ClassDB::bind_method(D_METHOD("get_energy_multiplier"), &WorldReflectionProbe::get_energy_multiplier);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "energy_multiplier", PROPERTY_HINT_RANGE, "0.0,10.0,0.01,5.0"), "set_energy_multiplier", "get_energy_multiplier");

	ClassDB::bind_method(D_METHOD("set_energy_exponent"), &WorldReflectionProbe::set_energy_exponent);
	ClassDB::bind_method(D_METHOD("get_energy_exponent"), &WorldReflectionProbe::get_energy_exponent);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "energy_exponent", PROPERTY_HINT_EXP_EASING), "set_energy_exponent", "get_energy_exponent");
}
