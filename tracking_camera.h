#ifndef TRACKING_CAMERA_H_8a1163822a1011e8b4670ed5f89f718b
#define TRACKING_CAMERA_H_8a1163822a1011e8b4670ed5f89f718b

#include <core/os/os.h>
#include <scene/3d/camera.h>

class TrackingCamera : public Camera {
	GDCLASS(TrackingCamera, Camera)

public:
	TrackingCamera();

	void reset_position();
	void set_target(const Spatial*);
	void set_target_node(const NodePath& n) { this->nodeTargetPath=n; this->find_node(); }
	_FORCE_INLINE_ NodePath get_target_node() const { return this->nodeTargetPath; }
	void set_enabled(bool e) { this->bIsEnabled=e; }
	_FORCE_INLINE_ bool is_enabled() const { return this->bIsEnabled; }
	void set_velocity_multiplier(Vector3 v) { this->vVelocityMultiplier=v; }
	_FORCE_INLINE_ Vector3 get_velocity_multiplier() const { return this->vVelocityMultiplier; }
	void set_time_multiplier(Vector3 t) { this->vTimeMultiplier=t; }
	_FORCE_INLINE_ Vector3 get_time_multiplier() const { return this->vTimeMultiplier; }
	void set_velocity_method(String m) { this->sVelocityMethod=m; this->find_node(); }
	_FORCE_INLINE_ String get_velocity_method() { return this->sVelocityMethod; }

	void set_anchor_offset(Vector3 o) { this->vAnchorOffset=o; }
	_FORCE_INLINE_ Vector3 get_anchor_offset() const { return this->vAnchorOffset; }

protected:
	void _notification(int);
	static void _bind_methods();
	void _set_target(const Object*);

private:
	void translate_camera(Vector3, Vector3, real_t);
	void find_node();

	NodePath nodeTargetPath;
	Spatial *nodeTarget;
	bool bIsEnabled;
	Vector3 vVelocityMultiplier;
	Vector3 vTimeMultiplier;
	Vector3 vAnchorOffset;
	String sVelocityMethod;

	Vector3 vAnchor;
	Vector3 vPrevNodePos;
};

#endif	// TRACKING_CAMERA_H_8a1163822a1011e8b4670ed5f89f718b
