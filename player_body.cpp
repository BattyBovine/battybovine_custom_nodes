#include "player_body.h"

PlayerBody::PlayerBody()
{
	this->set_physics_process(true);

	this->csMovementState = NULL;

	//this->add_child(this->rcJumpBufferLeft);
	//this->rcJumpBufferLeft->set_owner(this);
	//this->add_child(this->rcJumpBufferRight);
	//this->rcJumpBufferRight->set_owner(this);
	this->bIsNearGround = false;

	this->fAcceleration = 6.0f;
	this->fGroundFriction = 9.5f;
	this->fAirFriction = 1.5f;
	this->fJumpHeight = 17.25f;
	this->fImpulseStrength = 0.25f;
	this->fGravityStrength = 5.25f;
}

void PlayerBody::_notification(int p_what)
{
	switch(p_what) {
		case NOTIFICATION_ENTER_TREE:
		{
			if(Engine::get_singleton()->is_editor_hint())
				this->set_physics_process(false);
		} break;
		case NOTIFICATION_PROCESS:
		{
			//this->bIsNearGround = (this->rcJumpBufferLeft->is_colliding() || this->rcJumpBufferRight->is_colliding());
		} break;
		case NOTIFICATION_PHYSICS_PROCESS:
		{
			if(this->csMovementState) {
				CharacterState *newstate = this->csMovementState->update(this->get_physics_process_delta_time());
				if(newstate) {
					delete this->csMovementState;
					this->csMovementState = newstate;
				}
			}
			Transform trans = this->get_transform();
			trans.origin.z = 0.0f;
			this->set_transform(trans);
		} break;
	}
}



void PlayerBody::reset_to_ground()
{
	this->set_translation(Vector3());
	this->vVelocity = Vector3();
}
bool PlayerBody::is_near_ground()
{
	return this->bIsNearGround;
}

void PlayerBody::set_velocity(Vector3 vel)
{
	this->vVelocity = vel;
}
Vector3 PlayerBody::get_velocity() const
{
	return this->vVelocity;
}

void PlayerBody::set_acceleration(const real_t &a)
{
	this->fAcceleration = a;
}
real_t PlayerBody::get_acceleration() const
{
	return this->fAcceleration;
}

void PlayerBody::set_ground_friction(const real_t &gf)
{
	this->fGroundFriction = gf;
}
real_t PlayerBody::get_ground_friction() const
{
	return this->fGroundFriction;
}

void PlayerBody::set_air_friction(const real_t &af)
{
	this->fAcceleration = af;
}
real_t PlayerBody::get_air_friction() const
{
	return this->fAirFriction;
}

void PlayerBody::set_jump_height(const real_t &jh)
{
	this->fJumpHeight = jh;
}
real_t PlayerBody::get_jump_height() const
{
	return this->fJumpHeight;
}

void PlayerBody::set_impulse_strength(const real_t &is)
{
	this->fImpulseStrength = is;
}
real_t PlayerBody::get_impulse_strength() const
{
	return this->fImpulseStrength;
}

void PlayerBody::set_gravity_strength(const real_t &gs)
{
	this->fGravityStrength = gs;
}
real_t PlayerBody::get_gravity_strength() const
{
	return this->fGravityStrength;
}



void PlayerBody::_bind_methods()
{
	ClassDB::bind_method(D_METHOD("reset_to_ground"), &PlayerBody::reset_to_ground);
	ClassDB::bind_method(D_METHOD("is_near_ground"), &PlayerBody::is_near_ground);

	ClassDB::bind_method(D_METHOD("get_velocity"), &PlayerBody::get_velocity);

	ADD_GROUP("Physics", "");
	ClassDB::bind_method(D_METHOD("set_acceleration"), &PlayerBody::set_acceleration);
	ClassDB::bind_method(D_METHOD("get_acceleration"), &PlayerBody::get_acceleration);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "acceleration", PROPERTY_HINT_RANGE, "0.0, 100.0, 0.01, 6.0"), "set_acceleration", "get_acceleration");

	ClassDB::bind_method(D_METHOD("set_ground_friction"), &PlayerBody::set_ground_friction);
	ClassDB::bind_method(D_METHOD("get_ground_friction"), &PlayerBody::get_ground_friction);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "ground_friction", PROPERTY_HINT_RANGE, "0.0, 100.0, 0.01, 9.5"), "set_ground_friction", "get_ground_friction");

	ClassDB::bind_method(D_METHOD("set_air_friction"), &PlayerBody::set_air_friction);
	ClassDB::bind_method(D_METHOD("get_air_friction"), &PlayerBody::get_air_friction);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "air_friction", PROPERTY_HINT_RANGE, "0.0, 100.0, 0.01, 1.5"), "set_air_friction", "get_air_friction");

	ClassDB::bind_method(D_METHOD("set_jump_height"), &PlayerBody::set_jump_height);
	ClassDB::bind_method(D_METHOD("get_jump_height"), &PlayerBody::get_jump_height);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "jump_height", PROPERTY_HINT_RANGE, "0.0, 100.0, 0.01, 17.25"), "set_jump_height", "get_jump_height");

	ClassDB::bind_method(D_METHOD("set_impulse_strength"), &PlayerBody::set_impulse_strength);
	ClassDB::bind_method(D_METHOD("get_impulse_strength"), &PlayerBody::get_impulse_strength);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "impulse_strength", PROPERTY_HINT_RANGE, "0.0, 100.0, 0.01, 0.25"), "set_impulse_strength", "get_impulse_strength");

	ClassDB::bind_method(D_METHOD("set_gravity_strength"), &PlayerBody::set_gravity_strength);
	ClassDB::bind_method(D_METHOD("get_gravity_strength"), &PlayerBody::get_gravity_strength);
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "gravity_strength", PROPERTY_HINT_RANGE, "0.0, 100.0, 0.01, 5.25"), "set_gravity_strength", "get_gravity_strength");
}



//ControlState::ControlState(PlayerBody *pb, Vector3 movementcarry) : CharacterState(pb)
//{
//	this->vMovementMagnitude = movementcarry;
//}
//void ControlState::handle_input(Ref<InputEvent> e)
//{
//	if(e->is_action("player_move_left") || e->is_action("player_move_right")) {
//		Ref<InputEventJoypadMotion> jm = e;
//		if(jm.is_valid()) {
//			float axisvalue = jm->get_axis_value();
//			if(axisvalue >= LEFT_JOYSTICK_DEADZONE)
//				this->vMovementMagnitude.x = (axisvalue-LEFT_JOYSTICK_DEADZONE) / (1.0f-LEFT_JOYSTICK_DEADZONE);
//			else if(axisvalue <= -LEFT_JOYSTICK_DEADZONE)
//				this->vMovementMagnitude.x = (axisvalue+LEFT_JOYSTICK_DEADZONE) / (1.0f-LEFT_JOYSTICK_DEADZONE);
//			else
//				this->vMovementMagnitude.x = 0.0f;
//		} else {
//			this->vMovementMagnitude.x = 0.0f;
//			if(e->is_action_pressed("player_move_left"))
//				this->vMovementMagnitude.x = -1.0f;
//			else if(e->is_action_pressed("player_move_right"))
//				this->vMovementMagnitude.x = 1.0f;
//		}
//	}
//}
//CharacterState *ControlState::update(float delta)
//{
//	this->control_update(delta);
//	this->apply_movement();
//	if(this->bIsJumpBuffered)
//		return new JumpingState(this->player, this->vMovementMagnitude);
//	if(!this->player->is_on_floor())
//		return new FreefallState(this->player, this->vMovementMagnitude);
//	return NULL;
//}
//void ControlState::control_update(float delta)
//{
//	Vector3 playervelocity = this->player->get_velocity();
//	float storedy = playervelocity.y;
//	if(this->vMovementMagnitude.length()==0.0f) {
//		if(this->player->is_on_floor())
//			playervelocity = playervelocity.linear_interpolate(Vector3(), this->player->get_ground_friction() * delta);
//		else
//			playervelocity = playervelocity.linear_interpolate(Vector3(), this->player->get_air_friction() * delta);
//	} else {
//		Vector3 heading;
//		Basis aim = this->player->get_global_transform().basis;
//		heading += aim[0];
//		heading = heading.normalized() * this->vMovementMagnitude;
//		Vector3 target = (heading * this->player->get_acceleration());
//		playervelocity = playervelocity.linear_interpolate(target, this->player->get_acceleration() * delta);
//	}
//	playervelocity.y = storedy;
//	this->player->set_velocity(playervelocity);
//}
//void ControlState::apply_movement()
//{
//	Vector3 playervelocity = this->player->get_velocity();
//	this->player->move_and_slide(playervelocity, Vector3(0.0f, 1.0f, 0.0f));
//
//	int slidecount = this->player->get_slide_count();
//	for(unsigned int index=0; index<slidecount; index++) {
//		KinematicBody::Collision collision = this->player->get_slide_collision(index);
//		RigidBody *rb = Object::cast_to<RigidBody>(ObjectDB::get_instance(collision.collider));
//		if(rb) {
//			rb->set_sleeping(false);
//			Vector3 impulse = -collision.normal * this->player->get_impulse_strength() * playervelocity.normalized().abs();
//			if(playervelocity.dot(impulse) < 0.0f)
//				impulse = -impulse;
//			if(playervelocity.y < 0.0f || this->player->is_on_floor())
//				impulse.y = 0.0f;
//			impulse.z = 0.0f;
//			rb->apply_impulse(collision.collision, impulse);
//		}
//	}
//}
//
//FreefallState::FreefallState(PlayerBody *pb, Vector3 movementcarry) : ControlState(pb, movementcarry)
//{
//	this->vMovementMagnitude = movementcarry;
//	this->fGravity = (float(ProjectSettings::get_singleton()->get_setting("physics/3d/default_gravity"))) * this->player->get_gravity_strength();
//	if(this->player->is_on_floor())
//		this->fJumpForgivenessTimer = 0.0f;
//	Vector3 playervelocity = this->player->get_velocity();
//	if(playervelocity.y > 0.0f)
//		playervelocity.y = 0.0f;
//	this->player->set_velocity(playervelocity);
//}
//void FreefallState::handle_input(Ref<InputEvent> e)
//{
//	ControlState::handle_input(e);
//	if(e->is_action_pressed("player_jump") && (this->player->is_on_floor() || this->player->is_near_ground() || this->fJumpForgivenessTimer < FORGIVENESS_TIMER_LIMIT))
//		this->bIsJumpBuffered = true;
//	else if(e->is_action_released("player_jump"))
//		this->bIsJumpBuffered = false;
//}
//CharacterState *FreefallState::update(float delta)
//{
//	this->physics_update(delta);
//	if(this->player->is_on_floor()) {
//		if(this->bIsJumpBuffered)
//			return new JumpingState(this->player, this->vMovementMagnitude);
//		Vector3 playervelocity = this->player->get_velocity();
//		playervelocity.y = this->fJumpForgivenessTimer = 0.0f;
//		this->player->set_velocity(playervelocity);
//	} else {
//		if(this->bIsJumpBuffered && this->fJumpForgivenessTimer < FORGIVENESS_TIMER_LIMIT)
//			return new JumpingState(this->player, this->vMovementMagnitude);
//		this->fJumpForgivenessTimer += delta;
//	}
//	return NULL;
//}
//void FreefallState::physics_update(float delta)
//{
//	this->control_update(delta);
//	Vector3 playervelocity = this->player->get_velocity();
//	playervelocity.y -= this->fGravity * delta;
//	if(playervelocity.y < TERMINAL_VELOCITY)
//		playervelocity.y = TERMINAL_VELOCITY;
//	this->player->set_velocity(playervelocity);
//	this->apply_movement();
//}
//
//JumpingState::JumpingState(PlayerBody *pb, Vector3 movementcarry) : FreefallState(pb, movementcarry)
//{
//	Vector3 playervelocity = this->player->get_velocity();
//	playervelocity.y = this->player->get_jump_height();
//	this->player->set_velocity(playervelocity);
//}
//void JumpingState::handle_input(Ref<InputEvent> e)
//{
//	float playerjumpheightcutoff = this->player->get_jump_height() / 2.0f;
//	Vector3 playervelocity = this->player->get_velocity();
//	if(e->is_action_released("player_jump") && playervelocity.y > playerjumpheightcutoff) {
//		playervelocity.y = playerjumpheightcutoff;
//		this->player->set_velocity(playervelocity);
//	}
//	FreefallState::handle_input(e);
//}
//CharacterState *JumpingState::update(float delta)
//{
//	this->physics_update(delta);
//	if(this->player->get_velocity().y <= 0.0f || this->player->is_on_ceiling())
//		return new FreefallState(this->player, this->vMovementMagnitude);
//	return NULL;
//}
