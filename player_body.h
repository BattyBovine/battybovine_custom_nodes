#ifndef PLAYER_BODY_H_b037807e2b4a11e8b4670ed5f89f718b
#define PLAYER_BODY_H_b037807e2b4a11e8b4670ed5f89f718b

#include <core/os/os.h>
#include <scene/3d/physics_body.h>
#include <scene/3d/ray_cast.h>

//#define TRACKING_CAMERA_DEBUG

#define TERMINAL_VELOCITY		-20.0f
#define LEFT_JOYSTICK_DEADZONE	0.05f
#define FORGIVENESS_TIMER_LIMIT	0.1f

class PlayerBody : public KinematicBody {
	GDCLASS(PlayerBody, KinematicBody)
	friend class CharacterState;

public:
	PlayerBody();

	void reset_to_ground();
	bool is_near_ground();

	void set_velocity(Vector3);
	Vector3 get_velocity() const;

	void set_acceleration(const real_t&);
	real_t get_acceleration() const;
	void set_ground_friction(const real_t&);
	real_t get_ground_friction() const;
	void set_air_friction(const real_t&);
	real_t get_air_friction() const;
	void set_jump_height(const real_t&);
	real_t get_jump_height() const;
	void set_impulse_strength(const real_t&);
	real_t get_impulse_strength() const;
	void set_gravity_strength(const real_t&);
	real_t get_gravity_strength() const;

protected:
	void _notification(int);
	static void _bind_methods();

private:
	Vector3 vVelocity;

	CharacterState *csMovementState;

	RayCast *rcJumpBufferLeft;
	RayCast *rcJumpBufferRight;
	bool bIsNearGround;

	float fAcceleration;
	float fGroundFriction;
	float fAirFriction;
	float fJumpHeight;
	float fImpulseStrength;
	float fGravityStrength;

	#ifdef TRACKING_CAMERA_DEBUG
	OS *os;
	#endif
};


class CharacterState : public Object
{
public:
	CharacterState(PlayerBody *pb) { this->player = pb; }
	virtual void handle_input(Ref<InputEvent>) = 0;
	virtual CharacterState *update(float) = 0;
protected:
	PlayerBody *player = NULL;
};

//class ControlState : public CharacterState
//{
//protected:
//	virtual void control_update(float);
//	virtual void apply_movement();
//	Vector3 vMovementMagnitude;
//	bool bIsJumpBuffered = false;
//public:
//	ControlState(PlayerBody*, Vector3 movementcarry=Vector3());
//	void handle_input(Ref<InputEvent>);
//	CharacterState *update(float);
//};
//
//class FreefallState : public ControlState
//{
//protected:
//	void physics_update(float);
//	float fGravity;
//	float fJumpForgivenessTimer = FORGIVENESS_TIMER_LIMIT;
//public:
//	FreefallState(PlayerBody*, Vector3 movementcarry = Vector3());
//	void handle_input(Ref<InputEvent>);
//	CharacterState *update(float);
//};
//
//class JumpingState : public FreefallState
//{
//public:
//	JumpingState(PlayerBody*, Vector3 movementcarry = Vector3());
//	void handle_input(Ref<InputEvent>);
//	CharacterState *update(float);
//};

#endif	// PLAYER_BODY_H_b037807e2b4a11e8b4670ed5f89f718b
