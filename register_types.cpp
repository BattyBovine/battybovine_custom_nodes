#include "register_types.h"
#include "tracking_camera.h"
#include "world_reflection_probe.h"


void register_battybovine_custom_nodes_types()
{
	ClassDB::register_class<TrackingCamera>();
	ClassDB::register_class<WorldReflectionProbe>();
}

void unregister_battybovine_custom_nodes_types()
{
}
